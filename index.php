<?php

require('animal.php');
require('frog.php');
require('ape.php');

$animal = new Animal("shaun");

echo "Nama Hewan : $animal->name <br>"; // "shaun"
echo "Jumlah Kaki : $animal->legs <br>"; // 2
echo "Berdarah Dingin : $animal->cold_blooded <br>"; // false
echo "<br>";
$frog = new Frog("frog");

echo "Nama Hewan : $frog->name <br>";
echo "Jumlah Kaki : $frog->legs <br>";
echo "Berdarah Dingin : $frog->cold_blooded <br>";
echo "<br>";
$ape = new Ape("ape");

echo "Nama Hewan : $ape->name <br>";
echo "Jumlah Kaki : $ape->legs <br>";
echo "Berdarah Dingin : $ape->cold_blooded <br>";
echo "<br>";
$sungokong = new Ape("kera sakti");
echo "Nama Hewan : $sungokong->name <br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "Nama Hewan : $kodok->name <br>";
$kodok->jump() ; // "hop hop"